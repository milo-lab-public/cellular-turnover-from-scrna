import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.cluster import hierarchy as hy
from scipy.stats import norm, expon
from scipy.optimize import minimize, minimize_scalar, curve_fit
import seaborn as sns
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
from sklearn.mixture import GaussianMixture
from statsmodels.stats.multitest import fdrcorrection
import warnings

glists_path = 'C:/Users/ronse/Dropbox (Weizmann Institute)/scRNA cell analysis/Sources/Genes lists/'
glists_fname_dicts = {
    'Whitfield': 'Whitfield_et_al_2002.xlsx',
    'GO': 'GO_0007049.xlsx',
    'Liu': 'Liu_2017_Table_S5.csv',
    'CB3': 'Cyclebase3_Human_Periodic.xlsx'}

save_dir = 'Figures'


def load_gene_list(glist_name='Whitfield'):
    fname = glists_fname_dicts[glist_name]
    format = fname.split('.')[1]
    if format == 'xlsx':
        cc_genes_df = pd.read_excel(glists_path + fname, index_col=0)
        if glist_name == 'GO':
            cc_genes_df = cc_genes_df.drop_duplicates('Symbol')
            cc_genes_df.index = cc_genes_df.Symbol
    elif format == 'csv':
        cc_genes_df = pd.read_csv(glists_path + fname, index_col=0)
    else:
        return np.nan
    cc_genes_df.index = cc_genes_df.index.str.upper()
    return cc_genes_df


def get_relevant_glist(ad, glist_name='Whitfield', thr=0.25):
    cc_genes_df = load_gene_list(glist_name)
    cc_genes_df['in_data'] = cc_genes_df.index.isin(ad.var_names)
    cc_genes_red = cc_genes_df[cc_genes_df.in_data].copy()
    cc_genes_red['above'] = False
    for phase in cc_genes_red.Phase.unique():
        genes_in_phase = cc_genes_red.index[cc_genes_red.Phase == phase]
        df_in_phase = pd.DataFrame(ad[:, genes_in_phase].X.toarray(), columns=genes_in_phase)
        genes_above = df_in_phase.columns[df_in_phase.corrwith(df_in_phase.mean(axis=1)) > thr]
        cc_genes_red.loc[(cc_genes_red.Phase == phase) & (cc_genes_red.index.isin(genes_above)), 'above'] = True
    return cc_genes_red[cc_genes_red.above].drop(['in_data'], axis=1)


def get_reduced_exp_mat(ad, glist_name='GO', thr=0.25):
    cc_genes_df = load_gene_list(glist_name)
    if glist_name == 'GO':
        cc_genes = cc_genes_df.Symbol.str.upper().unique()
    else:
        cc_genes = cc_genes_df.index.str.upper().unique()
    genes_in_list_flags = ad.var_names.isin(cc_genes)
    genes_in_list = ad.var_names[genes_in_list_flags]
    df_by_glist = pd.DataFrame(ad[:, genes_in_list_flags].X.toarray(), columns=genes_in_list)
    df_by_glist.index = ad.obs.index
    df_by_glist.columns.name = ad.obs.tissue[0]
    corr_mat = df_by_glist.corr()
    corr_mat_no_diag = pd.DataFrame(corr_mat - np.diag(np.diag(corr_mat)), index=genes_in_list)
    cc_genes_red = df_by_glist.columns[(corr_mat_no_diag.max() > thr)]
    return df_by_glist[cc_genes_red]


def embed_exp_mat(df_red, n_pca=4, n_tsne=2):
    pca = PCA(n_components=n_pca)
    pca.fit(df_red)
    x_pca = pca.transform(df_red)
    if n_tsne>0:
        return TSNE(n_components=n_tsne).fit_transform(x_pca)
    else:
        return x_pca


def get_ordered_clusters(df_red, n_pca=4, n_tsne=2, c_metric='euclidean', min_cluster_size=25,
                         init_clust_num=40, Z_hy=None, save_Z=True, dir_save='Results/Linkage'):
    x_embedded = embed_exp_mat(df_red, n_pca=n_pca, n_tsne=n_tsne)
    if Z_hy is None:
        Z = hy.linkage(x_embedded, method='average', metric=c_metric, optimal_ordering=True)
        if save_Z:
            save_name = '{}/{}-pca={:d},tsne={:d}.csv'.format(dir_save, df_red.columns.name, n_pca, n_tsne)
            np.savetxt(save_name, Z, delimiter=",")
    else:
        Z = Z_hy
    for cl_num in np.arange(init_clust_num, 2, -3):
        clusters = hy.fcluster(Z, cl_num, criterion='maxclust')
        cl_sizes, bins = np.histogram(clusters, bins=np.arange(cl_num + 1) + 0.5)
        if (cl_sizes < min_cluster_size).sum() < 3:
            return Z, clusters, hy.leaves_list(Z), x_embedded
    return Z, clusters, hy.leaves_list(Z), x_embedded


def extract_order_clust(ad, glist_name='GO', thr=0.25, c_metric='euclidean', Z_hy=None,
                        smoothness_th_dict={0.5: 0.05, 0.75: 0.1}, show_figure_smoothness=False):
    df_red = get_reduced_exp_mat(ad, glist_name=glist_name, thr=thr)
    Z, clusters, order, x_embedded = get_ordered_clusters(df_red, c_metric=c_metric,  Z_hy=Z_hy)
    ad.obs['cluster'] = clusters
    ad.obs['order'] = np.argsort(order)
    for i in np.arange(x_embedded.shape[1]):
        ad.obs['x{:d}_embedded'.format(i)] = x_embedded[:, i]
    sc_c = smoothness_score_of_cluster(ad, df_red)
    if show_figure_smoothness:
        plot_smoothness_estimate(sc_c, smoothness_th_dict=smoothness_th_dict)
        plt.savefig('{}/{}_smoothness_({}).png'.format(save_dir, df_red.columns.name, c_metric))
    sc_q = sc_c.fillna(0).quantile(tuple(smoothness_th_dict.keys())).T.values
    tile_th = np.tile(tuple(smoothness_th_dict.values()), (sc_q.shape[0], 1))
    clust_sm_score = dict(zip(sc_c.columns, (sc_q >= tile_th).sum(axis=1) / len(smoothness_th_dict)))
    ad.obs['clust_sm_score'] = ad.obs.cluster.map(clust_sm_score)
    return ad

def autocorr_1lag(x, lag=1):
    '''numpy.corrcoef, partial'''
    return np.corrcoef(x[lag:], x[:-lag])[0][1]


def gene_smoothness_score(df_ord_cl, N=50):
    sc_ord = df_ord_cl.apply(autocorr_1lag)
    sc_rand = 0
    for i in range(N):
        sc_rand += df_ord_cl.sample(frac=1, replace=False).apply(autocorr_1lag)
    sc_rand = sc_rand / N
    return sc_ord - sc_rand


def smoothness_score_of_cluster(ad, df_red):
    # ad_ord = ad[np.argsort(ad.obs.order), :].copy()
    ad_ord = ad[np.argsort(ad.obs.order)].copy()
    df_ord = df_red.loc[ad_ord.obs.index].copy()
    df_ord.index = ad_ord.obs.order
    sc_c = pd.DataFrame(0, index=df_ord.columns, columns=ad_ord.obs.cluster.unique())
    for cl in ad_ord.obs.cluster.unique():
        df_ord_cl = df_ord[(ad_ord.obs.cluster == cl).values]
        sc_c[cl] = gene_smoothness_score(df_ord_cl)
    return sc_c


def plot_smoothness_estimate(sc_c, smoothness_th_dict={0.5: 0.05, 0.75: 0.1}):
    fig, axes = plt.subplots(2, 1)
    fig.set_size_inches(10, 15)
    palette = 'Set2'
    sns.ecdfplot(data=sc_c.melt(value_name='Smoothness score', var_name='Cluster').fillna(0), x='Smoothness score',
                 hue='Cluster', alpha=0.65, ax=axes[0], palette=palette, stat="proportion")
    axes[0].vlines(smoothness_th_dict.values(), ymin=0, ymax=1, color='grey', ls='--', lw=1)
    axes[0].scatter(smoothness_th_dict.values(), smoothness_th_dict.keys(), color='grey', marker='_')

    sns.stripplot(data=sc_c.fillna(0), ax=axes[1], palette=palette)
    sns.boxplot(data=sc_c.fillna(0), showfliers=False, ax=axes[1], palette=palette)
    axes[1].set_xlabel('Cluster')
    axes[1].set_ylabel('Smoothness score')
    axes[1].hlines(smoothness_th_dict.values(), xmin=-0.5, xmax=sc_c.columns.max() - 0.5, color='grey', ls='--', lw=1)
    plt.tight_layout()

def get_phases_scores(ad, glist_name='Whitfield', corr_thr=0.25):
    df_only_above = get_relevant_glist(ad, glist_name=glist_name, thr=corr_thr)
    for phase in df_only_above.Phase.unique():
        ad.obs[phase] = ad[:, df_only_above.index[df_only_above.Phase == phase]].X.mean(axis=1)
    genes_phase_summ = df_only_above.groupby('Phase').sum()
    for phase in load_gene_list(glist_name).Phase.unique():
        if phase not in genes_phase_summ.index:
            genes_phase_summ.loc[phase] = 0
            ad.obs[phase] = 0

    ad.obs['max_score'] = ad.obs[df_only_above.Phase.unique()].max(axis=1)
    weights = genes_phase_summ.loc[df_only_above.Phase.unique(), 'above']
    ad.obs['mean_score'] = ad.obs[df_only_above.Phase.unique()].apply(lambda row: np.average(row, weights=weights),
                                                                      axis=1)
    weights = genes_phase_summ.loc[['G1/S', 'S'], 'above']
    ad.obs['mean_G1_S'] = ad.obs[['G1/S', 'S']].apply(lambda row: np.average(row, weights=weights), axis=1)
    weights = genes_phase_summ.loc[['G1/S', 'M/G1'], 'above']
    ad.obs['mean_G1'] = ad.obs[['G1/S', 'M/G1']].apply(lambda row: np.average(row, weights=weights), axis=1)
    weights = genes_phase_summ.loc[['G2/M', 'G2'], 'above']
    ad.obs['mean_G2_M'] = ad.obs[['G2/M', 'G2']].apply(lambda row: np.average(row, weights=weights), axis=1)
    return ad.obs


def norm_loss(mu, sig, x_th, x, alpha=0):
    n = x.shape[0]
    ecdf, bins = np.histogram(x, bins=300)
    x_bins = (bins[:-1] + bins[1:]) / 2
    lower_inds = (x_bins <= x_th)
    y_ecdf = np.cumsum(ecdf)
    max_bin = np.where(lower_inds)[0][-1]
    max_y = y_ecdf[max_bin]
    f = max_y / n
    y_ecdf = y_ecdf * lower_inds / n / f
    y_pred = norm(mu, sig).cdf(x_bins) * lower_inds
    # loss = sum(ecdf*(y_ecdf - y_pred) ** 2) + np.exp(alpha * (1 - f))
    loss = sum((y_ecdf - y_pred) ** 2) + np.exp(alpha * (1 - f))

    return loss


def loss_norm(mu, sig, x):
    n = x.shape[0]
    return - n / 2 * np.log(2 * np.pi * sig ** 2) - 1 / (2 * sig ** 2) * sum((x - mu) ** 2)


def llk_norm_x(x):
    return -loss_norm(x.mean(), x.std(), x)


def two_norm_loss(x_th, x):
    return llk_norm_x(x[x <= x_th]) + llk_norm_x(x[x > x_th])


def llk_exp(x):
    lam = 1 / x.mean()
    n = x.shape[0]
    return n * np.log(lam) - lam * x.sum()


def llk_exp_w_loc(x):
    loc, lam = expon.fit(x)
    # n = x.shape[0]
    # return n*np.log(lam) - lam*(x-loc).sum()
    return llk_exp(x - loc)


def norm_and_exp_loss(x_th, x):
    return llk_norm_x(x[x <= x_th]) + llk_exp(x[x > x_th])


def norm_and_exp_loss_w_loc(x_th, x):
    return llk_norm_x(x[x <= x_th]) + llk_exp_w_loc(x[x > x_th])


def loss_norm_w_th(mu, sig, x_th, x, alpha=0):
    n0 = x.shape[0]
    x2 = x[x <= x_th]
    n1 = x2.shape[0]
    # return - n1/2 * np.log(2*np.pi*sig**2) - 1/(2*sig**2)*sum((x2-mu)**2) - np.exp(alpha * (1 - n1/n0))
    # return - n1/2 * np.log(2*np.pi*sig**2) - 1/(2*sig**2)*sum((x2-mu)**2) - n1*(1-np.exp(alpha * - n1/n0))
    return - n1 / 2 * np.log(2 * np.pi * sig ** 2) - 1 / (2 * sig ** 2) * sum((x2 - mu) ** 2) - n1 * (
            1 - n1 / n0) ** alpha


def get_fdr_cc_scores(score_df, score_list=['max_score', 'mean_score', 'mean_G1', 'mean_G1_S', 'mean_G2_M'],
                      fdr_alpha=0.05):
    norm_fits = pd.DataFrame(index=score_list,
                             columns=['mu', 'sig', 'x_th', 'fdr_alpha', 'norm_fit', 'norm_th', 'fdr_th', 'mu_u',
                                      'sig_u', 'norm_u', 'frac_d', 'frac_u', 'x_th_hist', 'frac_hist',
                                      'mu_hist', 'sig_hist', 'hist_ext', 'norm_th_hist', 'fdr_th_hist'])
    norm_fits.fdr_alpha = fdr_alpha
    for score in score_list:
        # print(score)
        x = score_df[score]
        bounds = (x.min() + 0.001, x.max())
        gm = GaussianMixture(n_components=2)
        gm.fit(x.values.reshape(-1, 1))
        means = gm.means_.squeeze()
        stds = gm.covariances_.squeeze() ** 0.5
        noise_index = means.argmin()

        norm_fits.loc[score, ['mu', 'sig']] = means[noise_index], stds[noise_index]
        norm_fits.loc[score, 'norm_fit'] = norm(norm_fits.mu[score], norm_fits.sig[score])
        noise_th = norm_fits.norm_fit[score].ppf(0.999)
        norm_fits.loc[score, ['x_th']] = noise_th
        norm_fits.loc[score, ['mu_u', 'sig_u']] = means[1 - noise_index], stds[1 - noise_index]
        norm_fits.loc[score, 'norm_u'] = norm(norm_fits.mu_u[score], norm_fits.sig_u[score])
        norm_fits.loc[score, 'frac_u'] = x[x > noise_th].shape[0] / x.shape[0] / norm_fits.norm_u[score].sf(noise_th)
        norm_fits.loc[score, 'frac_d'] = 1 - norm_fits.loc[score, 'frac_u']
        score_df['pvalue_{}'.format(score)] = norm_fits.loc[score, 'norm_fit'].sf(x)
        # score_df['pvalue_{}'.format(score)] = gm.predict_proba(x.values.reshape(-1, 1))[:, noise_index]

        temp = fdrcorrection(score_df['pvalue_{}'.format(score)], alpha=fdr_alpha)
        score_df['fdr_flag_{}'.format(score)] = temp[0]
        score_df['fdr_{}'.format(score)] = temp[1]
        # score_df['fdr_{}'.format(score)] = gm.predict_proba(x.values.reshape(-1, 1))[:, noise_index]
        # score_df['fdr_flag_{}'.format(score)] = score_df['fdr_{}'.format(score)] <= fdr_alpha
        #

        norm_fits.loc[score, 'norm_th'] = norm_fits.loc[score, 'norm_fit'].ppf(1 - fdr_alpha)
        norm_fits.loc[score, 'fdr_th'] = score_df.loc[score_df['fdr_flag_{}'.format(score)], score].abs().min()

        epdf, bins = np.histogram(x[x < 1], density=True, bins=50)
        x_bins = (bins[1:] + bins[:-1]) / 2

        def gauss(x, x0, sigma):
            return norm(x0, sigma).pdf(x)

        # choosing a domain for that seems to represent the noise
        xmin, xmax = x_bins[1], 0.1
        norm_fits.loc[score, 'x_th_hist'] = xmax
        max_for_trap = xmax + 0.2  # assuming the integral of the histogram reflects more area

        fac = np.trapz(epdf[(xmin <= x_bins) & (x_bins <= max_for_trap)],
                       x_bins[(xmin <= x_bins) & (x_bins <= max_for_trap)])
        parameters, covariance = curve_fit(gauss, x_bins[(xmin <= x_bins) & (x_bins <= xmax)],
                                           epdf[(xmin <= x_bins) & (x_bins <= xmax)] / fac)
        norm_fits.loc[score, ['mu_hist', 'sig_hist']] = parameters
        norm_fits.loc[score, 'hist_ext'] = norm(loc=parameters[0], scale=parameters[1])
        score_df['pvalue_hist_{}'.format(score)] = norm_fits.loc[score, 'hist_ext'].sf(x)
        temp = fdrcorrection(score_df['pvalue_hist_{}'.format(score)], alpha=fdr_alpha)
        score_df['fdr_flag_hist_{}'.format(score)] = temp[0]
        score_df['fdr_hist_{}'.format(score)] = temp[1]
        norm_fits.loc[score, 'norm_th_hist'] = norm_fits.loc[score, 'hist_ext'].ppf(1 - fdr_alpha)
        norm_fits.loc[score, 'fdr_th_hist'] = score_df.loc[
            score_df['fdr_flag_hist_{}'.format(score)], score].abs().min()
        norm_fits.loc[score, 'frac_hist'] = x[x <= norm_fits.loc[score, 'hist_ext'].ppf(0.99)].shape[0] / x.shape[0]

    score_df['over_th'] = score_df.fdr_flag_mean_G1_S + 2 * score_df.fdr_flag_mean_G2_M
    map_dic = {0: 'not cycling', 1: 'G1/S', 2: 'G2/M', 3: 'G1/S + G2/M'}
    score_df['positive_for_cell_cycle'] = score_df.over_th.map(map_dic)
    score_df['over_th_hist'] = score_df.fdr_flag_hist_mean_G1_S + 2 * score_df.fdr_flag_hist_mean_G2_M
    score_df['positive_for_cell_cycle_hist'] = score_df.over_th_hist.map(map_dic)
    score_df['positive_by_mean'] = score_df.fdr_flag_mean_score.map({True: 'cycling', False: 'not cycling'})
    score_df['positive_by_mean_hist'] = score_df.fdr_flag_hist_mean_score.map({True: 'cycling', False: 'not cycling'})

    return score_df, norm_fits

#
# def get_fdr_cc_scores(score_df, score_list=['max_score', 'mean_score', 'mean_G1', 'mean_G1_S', 'mean_G2_M'],
#                       fdr_alpha=0.05):
#     norm_fits = pd.DataFrame(index=score_list,
#                              columns=['mu', 'sig', 'x_th', 'fdr_alpha', 'norm_fit', 'norm_th', 'fdr_th', 'mu_u',
#                                       'sig_u', 'norm_u', 'frac_d', 'frac_u', 'x_th_hist', 'frac_hist',
#                                       'mu_hist', 'sig_hist', 'hist_ext', 'norm_th_hist', 'fdr_th_hist'])
#     norm_fits.fdr_alpha = fdr_alpha
#     for score in score_list:
#         # print(score)
#         x = score_df[score]
#         bounds = (x.min() + 0.001, x.max())
#         x_th = minimize_scalar(lambda th: two_norm_loss(th, x), method='bounded', bounds=bounds).x
#         norm_fits.loc[score, ['mu', 'sig', 'x_th']] = x[x <= x_th].mean(), x[x <= x_th].std(), x_th
#         norm_fits.loc[score, 'norm_fit'] = norm(norm_fits.mu[score], norm_fits.sig[score])
#         norm_fits.loc[score, ['mu_u', 'sig_u']] = x[x > x_th].mean(), x[x > x_th].std()
#         norm_fits.loc[score, 'norm_u'] = norm(norm_fits.mu_u[score], norm_fits.sig_u[score])
#         norm_fits.loc[score, 'frac_d'] = x[x <= x_th].shape[0] / x.shape[0]
#         norm_fits.loc[score, 'frac_u'] = 1 - norm_fits.loc[score, 'frac_d']
#         score_df['pvalue_{}'.format(score)] = norm_fits.loc[score, 'norm_fit'].sf(x)
#         temp = fdrcorrection(score_df['pvalue_{}'.format(score)], alpha=fdr_alpha)
#         score_df['fdr_flag_{}'.format(score)] = temp[0]
#         score_df['fdr_{}'.format(score)] = temp[1]
#         norm_fits.loc[score, 'norm_th'] = norm_fits.loc[score, 'norm_fit'].ppf(1 - fdr_alpha)
#         norm_fits.loc[score, 'fdr_th'] = score_df.loc[score_df['fdr_flag_{}'.format(score)], score].abs().min()
#
#         epdf, bins = np.histogram(x[x < 1], density=True, bins=50)
#         x_bins = (bins[1:] + bins[:-1]) / 2
#
#         def gauss(x, x0, sigma):
#             return norm(x0, sigma).pdf(x)
#
#         # choosing a domain for that seems to represent the noise
#         xmin, xmax = x_bins[1], 0.1
#         norm_fits.loc[score, 'x_th_hist'] = xmax
#         max_for_trap = xmax + 0.2  # assuming the integral of the histogram reflects more area
#
#         fac = np.trapz(epdf[(xmin <= x_bins) & (x_bins <= max_for_trap)],
#                        x_bins[(xmin <= x_bins) & (x_bins <= max_for_trap)])
#         parameters, covariance = curve_fit(gauss, x_bins[(xmin <= x_bins) & (x_bins <= xmax)],
#                                            epdf[(xmin <= x_bins) & (x_bins <= xmax)] / fac)
#         norm_fits.loc[score, ['mu_hist', 'sig_hist']] = parameters
#         norm_fits.loc[score, 'hist_ext'] = norm(loc=parameters[0], scale=parameters[1])
#         score_df['pvalue_hist_{}'.format(score)] = norm_fits.loc[score, 'hist_ext'].sf(x)
#         temp = fdrcorrection(score_df['pvalue_hist_{}'.format(score)], alpha=fdr_alpha)
#         score_df['fdr_flag_hist_{}'.format(score)] = temp[0]
#         score_df['fdr_hist_{}'.format(score)] = temp[1]
#         norm_fits.loc[score, 'norm_th_hist'] = norm_fits.loc[score, 'hist_ext'].ppf(1 - fdr_alpha)
#         norm_fits.loc[score, 'fdr_th_hist'] = score_df.loc[
#             score_df['fdr_flag_hist_{}'.format(score)], score].abs().min()
#         norm_fits.loc[score, 'frac_hist'] = x[x <= norm_fits.loc[score, 'hist_ext'].ppf(0.99)].shape[0] / x.shape[0]
#
#     score_df['over_th'] = score_df.fdr_flag_mean_G1_S + 2 * score_df.fdr_flag_mean_G2_M
#     map_dic = {0: 'not cycling', 1: 'G1/S', 2: 'G2/M', 3: 'G1/S + G2/M'}
#     score_df['positive_for_cell_cycle'] = score_df.over_th.map(map_dic)
#     score_df['over_th_hist'] = score_df.fdr_flag_hist_mean_G1_S + 2 * score_df.fdr_flag_hist_mean_G2_M
#     score_df['positive_for_cell_cycle_hist'] = score_df.over_th_hist.map(map_dic)
#     score_df['positive_by_mean'] = score_df.fdr_flag_mean_score.map({True: 'cycling', False: 'not cycling'})
#     score_df['positive_by_mean_hist'] = score_df.fdr_flag_hist_mean_score.map({True: 'cycling', False: 'not cycling'})
#
#     return score_df, norm_fits


# def get_fdr_cc_scores(score_df, score_list=['max_score', 'mean_score', 'mean_G1', 'mean_G1_S', 'mean_G2_M'],
#                       fdr_alpha=0.05, fraction_weight=0):
#     norm_fits = pd.DataFrame(index=score_list,
#                              columns=['mu', 'sig', 'x_th', 'fdr_alpha', 'norm_fit', 'norm_th', 'fdr_th'])
#     norm_fits.fdr_alpha = fdr_alpha
#     for score in score_list:
#         print(score)
#         x = score_df[score]
#         with warnings.catch_warnings():
#             warnings.simplefilter("ignore")
#             res = minimize(
#                 fun=lambda theta: norm_loss(mu=theta[0], sig=theta[1], x_th=theta[2], x=x, alpha=fraction_weight),
#                 x0=(0.01, 0.2, 0.5),
#                 method='Nelder-Mead',
#                 options={'gtol': 1e-6, 'disp': True}
#             )
#         norm_fits.loc[score, ['mu', 'sig', 'x_th']] = res.x
#         norm_fits.loc[score, 'norm_fit'] = norm(res.x[0], res.x[1])
#         score_df['pvalue_{}'.format(score)] = norm_fits.loc[score, 'norm_fit'].sf(x)
#         temp = fdrcorrection(score_df['pvalue_{}'.format(score)], alpha=fdr_alpha)
#         score_df['fdr_flag_{}'.format(score)] = temp[0]
#         score_df['fdr_{}'.format(score)] = temp[1]
#         norm_fits.loc[score, 'norm_th'] = norm_fits.loc[score, 'norm_fit'].ppf(1 - fdr_alpha)
#         norm_fits.loc[score, 'fdr_th'] = score_df.loc[score_df['fdr_flag_{}'.format(score)], score].abs().min()
#     score_df['over_th'] = score_df.fdr_flag_mean_G1_S + 2 * score_df.fdr_flag_mean_G2_M
#     map_dic = {0: 'not cycling', 1: 'G1/S', 2: 'G2/M', 3: 'G1/S + G2/M'}
#     score_df['positive_for_cell_cycle'] = score_df.over_th.map(map_dic)
#     return score_df, norm_fits


def positive_vs_fdr(x_fdr, fdr_bins=np.logspace(-20, 0, 201), ax=None, fdr_alpha=None, **kw):
    if ax is None:
        plt.figure()
        ax = plt.gca()
    ecdf, bins = np.histogram(x_fdr, bins=fdr_bins)
    y_ecdf = np.cumsum(ecdf) + x_fdr.shape[0] - ecdf.sum()
    if ax:
        ax.plot(bins[1:], y_ecdf, **kw)
        ax.set_xscale('log')
        if fdr_alpha is not None:
            ax.scatter(fdr_alpha, (x_fdr <= fdr_alpha).sum(), s=50, color='k', marker='*', zorder=100)
        ax.set_xlabel('FDR alpha')
        ax.set_ylabel('Cells over threshold ')
        ax.legend(loc='upper left')
        bounds = [10 ** -8, 0.3]
        ax.set_xlim(bounds[0], bounds[1])
        # print(np.ceil((x_fdr <= bounds[1]).sum(), -2))
        ax.set_ylim(0, 100 * np.ceil((x_fdr <= bounds[1]).sum() / 100))

    return y_ecdf


def plot_phases_positive_vs_fdr(scores_df, method='two_norms', fdr_bins=np.logspace(-20, 0, 201), ax=None,
                                fdr_alpha=None):
    if ax is None:
        plt.figure()
        ax = plt.gca()
    if method == 'two_norms':
        suf = ''
    else:
        suf = '_hist'
    # hue = '{}{}'.format(hue,suf)
    g1_s = 'fdr{}_mean_G1_S'.format(suf)
    g2_m = 'fdr{}_mean_G2_M'.format(suf)
    fdr = scores_df[scores_df[g1_s] <= scores_df[g2_m]][g1_s]
    # fdr = scores_df[scores_df.fdr_mean_G1_S >= scores_df.fdr_mean_G2_M].fdr_mean_G1_S
    ecdf_G1_S = positive_vs_fdr(fdr, fdr_alpha=fdr_alpha, fdr_bins=fdr_bins, label='G1_S', ls='--', alpha=0.65, lw=2,
                                ax=ax, color='C1')
    fdr = scores_df[scores_df[g1_s] > scores_df[g2_m]][g2_m]
    # fdr = scores_df[scores_df.fdr_mean_G2_M > scores_df.fdr_mean_G1_S].fdr_mean_G2_M
    ecdf_G2_M = positive_vs_fdr(fdr, fdr_alpha=fdr_alpha, fdr_bins=fdr_bins, label='G2_M', ls='--', alpha=0.65, lw=2,
                                ax=ax, color='C3')
    fdr = scores_df['fdr{}_mean_score'.format(suf)]
    ecdf_mean = positive_vs_fdr(fdr, fdr_alpha=fdr_alpha, fdr_bins=fdr_bins, label='mean', ls='-', alpha=0.85, lw=4,
                                ax=ax, color='k')
    sum_ecdf = ecdf_G1_S + ecdf_G2_M
    ax.plot(fdr_bins[1:], sum_ecdf, label='G1_S + G2_M', ls='-', alpha=0.85, lw=4, color='navy')
    ax.scatter(fdr_alpha, sum_ecdf[fdr_bins[1:] == fdr_alpha], s=50, color='k', marker='*', zorder=100)
    ax.legend(loc='upper left')
    bounds = [10 ** -8, 0.1]
    ax.set_xlim(bounds[0], bounds[1])
    ax.set_ylim(0, 100 * np.ceil(sum_ecdf[np.where(fdr_bins < bounds[1])[0][-2]] / 100))
    ax.set_title('method = {}'.format(method.replace('_', ' ')))


def plot_positive_vs_fdr_comparison(scores_df, fdr_alpha=None, tissue_name='', save_flag=False):
    fig, axes = plt.subplots(2, 1)
    fig.set_size_inches(8, 12)
    fdr_bins = np.logspace(-20, 0, 201)
    plot_phases_positive_vs_fdr(scores_df, method='two_norms', fdr_bins=fdr_bins, ax=axes[0],
                                fdr_alpha=fdr_alpha)
    plot_phases_positive_vs_fdr(scores_df, method='hist_extrapolation', fdr_bins=fdr_bins, ax=axes[1],
                                fdr_alpha=fdr_alpha)
    meth = scores_df.method[0]
    fig.suptitle('{} {}'.format(tissue_name,meth), fontsize=20)
    plt.tight_layout()
    if save_flag:
        plt.savefig('{}/{} {} - positive_vs_fdr.png'.format(save_dir, tissue_name, meth))


def plot_cc_scores(score_df, norm_fits, score, tissue_name='', save_flag=False):
    fdr_alpha = norm_fits.fdr_alpha.iloc[0]
    fig, axes = plt.subplots(2, 2)
    fig.set_size_inches(15, 15)
    xx = np.linspace(-1, 3, 200)
    score_df[score].hist(bins=100, cumulative=True, density=True, alpha=0.2, ax=axes[0, 0])
    n_cells = score_df.shape[0]
    x_th = norm_fits.loc[score, 'x_th']
    axes[0, 0].vlines(x_th, 0, 1, 'darkgrey', ls='--')
    # factor = (score_df[score] < x_th).sum()
    axes[0, 0].plot(xx, norm_fits.norm_fit[score].cdf(xx) * norm_fits.frac_d[score], 'maroon', lw=4, zorder=20,
                    alpha=0.95,
                    label='two_normal (only noise)')
    axes[0, 0].plot(xx, norm_fits.hist_ext[score].cdf(xx) * norm_fits.frac_hist[score], 'darkgreen', lw=4, zorder=20,
                    alpha=0.95,
                    label='hist normal extrapolation')
    two_norms = norm_fits.norm_fit[score].cdf(xx) * norm_fits.frac_d[score] + norm_fits.norm_u[score].cdf(xx) * \
                norm_fits.frac_u[score]
    axes[0, 0].plot(xx, two_norms, 'grey', lw=2, zorder=30, alpha=0.7, label='two_normal')
    # axes[0, 0].vlines(norm_fits.loc[score, 'norm_th'], 0, 1, color='orange', label='norm threshold')
    axes[0, 0].vlines(norm_fits.loc[score, 'fdr_th'], 0, 1, color='salmon', label='fdr threshold (two normals)')
    axes[0, 0].vlines(norm_fits.loc[score, 'fdr_th_hist'], 0, 1, color='lightgreen',
                      label='fdr threshold (hist extrapolation)')
    axes[0, 0].set_xlim(-1, 3)
    axes[0, 0].set_xlabel(score)
    axes[0, 0].set_ylabel('Cumulative fraction of cells')

    score_df[score].hist(bins=100, cumulative=False, density=True, alpha=0.2, ax=axes[1, 0])
    y = norm_fits.norm_fit[score].pdf(xx) * norm_fits.frac_d[score]
    axes[1, 0].plot(xx, y, 'maroon', lw=4, zorder=20, alpha=0.95, label='two_normal (only noise)')
    axes[1, 0].plot(xx, norm_fits.hist_ext[score].pdf(xx) * norm_fits.frac_hist[score], 'darkgreen', lw=4, zorder=20,
                    alpha=0.95,
                    label='hist normal extrapolation')
    two_norms = norm_fits.norm_fit[score].pdf(xx) * norm_fits.frac_d[score] + norm_fits.norm_u[score].pdf(xx) * \
                norm_fits.frac_u[score]
    axes[1, 0].plot(xx, two_norms, 'grey', lw=2, zorder=30, alpha=0.7, label='two_normal')
    axes[1, 0].vlines(x_th, 0, y.max(), 'darkgrey', ls='--')
    axes[1, 0].vlines(norm_fits.loc[score, 'fdr_th'], 0, y.max(), color='salmon', label='fdr threshold (two normals)')
    axes[1, 0].vlines(norm_fits.loc[score, 'fdr_th_hist'], 0, y.max(), color='lightgreen',
                      label='fdr threshold (hist extrapolation)')
    axes[1, 0].set_xlim(-1, 3)
    axes[1, 0].set_xlabel(score)
    axes[1, 0].set_ylabel('probability density function')

    sns.scatterplot(data=score_df.sort_values(score), x=np.arange(n_cells), y=score, hue='pvalue_{}'.format(score),
                    style='fdr_flag_' + score, palette='bone', ax=axes[0, 1])
    axes[0, 1].hlines(norm_fits.loc[score, 'fdr_th'], 0, n_cells, color='salmon', label='fdr threshold (two normals)')
    axes[0, 1].hlines(norm_fits.loc[score, 'fdr_th_hist'], 0, n_cells, color='lightgreen',
                      label='fdr threshold (hist extrapolation)')
    axes[0, 1].legend()
    axes[0, 1].set_xlabel('Cell ordered by {}'.format(score))

    fdr = score_df['fdr_{}'.format(score)]
    positive_vs_fdr(fdr, fdr_alpha=fdr_alpha, ls='-', label='two norms', alpha=0.95, ax=axes[1, 1], color='maroon')
    fdr = score_df['fdr_hist_{}'.format(score)]
    positive_vs_fdr(fdr, fdr_alpha=fdr_alpha, ls='-', label='hist extrapolation', alpha=0.95, ax=axes[1, 1],
                    color='darkgreen')

    frac = score_df['fdr_flag_{}'.format(score)].sum() / n_cells
    frac_hist = score_df['fdr_flag_hist_{}'.format(score)].sum() / n_cells
    meth = score_df.method[0]
    title = '{} ({}) {}: {:0.1%} / {:0.1%} of cells are over threshold [alpha={:0.0e}]'.format(tissue_name, meth, score,
                                                                                               frac, frac_hist,
                                                                                               fdr_alpha)
    fig.suptitle(title, fontsize=20)
    fig.tight_layout(rect=[0, 0.02, 1, 0.97])
    if save_flag:
        plt.savefig('{}/{} - {} ({}).png'.format(save_dir, tissue_name, score, meth))


def scatter_phase_score_by(score_df, norm_fits, method='two_norms', hue='positive_for_cell_cycle',
                           by='cell_ontology_class', tissue_name='', save_flag=False):
    if method == 'two_norms':
        suf = ''
    else:
        suf = '_hist'
    hue = '{}{}'.format(hue, suf)
    # print(hue)
    plt.figure(figsize=(14, 8))
    fdr_alpha = norm_fits.fdr_alpha.iloc[0]
    if 'positive_for_cell_cycle' in hue:
        hue_order = ['not cycling', 'G1/S', 'G1/S + G2/M', 'G2/M']
    else:
        hue_order = ['not cycling', 'cycling']
    sns.scatterplot(data=score_df, x='mean_G1_S', y='mean_G2_M', hue=hue, style=by, hue_order=hue_order)
    x_bounds = (score_df.mean_G1_S.min() - 0.1, score_df.mean_G1_S.max() + 0.1)
    y_bounds = (score_df.mean_G2_M.min() - 0.1, score_df.mean_G2_M.max() + 0.1)
    ph_pos_fracs = score_df.groupby(hue).count()['cell'] / score_df.shape[0]
    handles, labels = plt.gca().get_legend_handles_labels()
    labels2 = labels.copy()
    labels2[0] = labels[0].replace('_', ' ')
    for i in range(1, labels.index(by)):
        labels2[i] = '{} ({:0.1%})'.format(labels[i], ph_pos_fracs[labels[i]])
    ct_pos_fracs = 1 - score_df[score_df[hue] == 'not cycling'].groupby(by).count()['cell'] / \
                   score_df.groupby(by).count()['cell']
    ct_pos_num = score_df.groupby(by).count()['cell']
    labels2[labels.index(by)] = labels[labels.index(by)].replace('_', ' ')
    for i in range(labels.index(by) + 1, len(labels)):
        labels2[i] = '{} ({:0.1%}, n={:0.0f})'.format(labels[i], ct_pos_fracs[labels[i]], ct_pos_num[labels[i]])

    plt.hlines(norm_fits.loc['mean_G2_M', 'fdr_th{}'.format(suf)], x_bounds[0], x_bounds[1], ls='--', alpha=0.85,
               color='grey', lw=3, label='G2_M threshold\n[alpha={:0.0e}]'.format(fdr_alpha))
    plt.vlines(norm_fits.loc['mean_G1_S', 'fdr_th{}'.format(suf)], y_bounds[0], y_bounds[1], ls='--', alpha=0.55,
               color='grey', lw=3, label='G1_S threshold\n[alpha={:0.0e}]'.format(fdr_alpha))
    plt.xlim(x_bounds)
    plt.ylim(y_bounds)
    plt.legend(labels=labels2, handles=handles, loc='center left', bbox_to_anchor=(1.01, 0.5))
    plt.xlabel('G1/S mean score')
    plt.ylabel('G2/M mean score')
    meth = score_df.method[0]
    plt.title('{} - {} [alpha={:0.0e}]'.format(tissue_name, meth, fdr_alpha))
    plt.tight_layout()
    if save_flag:
        plt.savefig('{}/{} {} {}, {} -alpha={}.png'.format(save_dir, tissue_name, meth, method, hue.split('_')[-1],
                                                           fdr_alpha))


res_dir = 'Results'
tissues = ['Aorta', 'BAT', 'Bladder', 'Brain_Myeloid', 'Brain_Non-Myeloid', 'Diaphragm', 'GAT', 'Heart', 'Kidney',
           'Large_Intestine', 'Limb_Muscle', 'Liver', 'Lung', 'Mammary_Gland', 'Marrow', 'MAT', 'Pancreas', 'SCAT',
           'Skin', 'Spleen', 'Thymus', 'Tongue', 'Trachea']


def combine_tissues_results(tissues=tissues, method='facs', suff=''):
    cc_cells = pd.DataFrame()
    for tissue_name in tissues:
        cc_cells = cc_cells.append(pd.read_csv('{}/{} - {}{}.csv'.format(res_dir, tissue_name, method, suff), index_col=0))
    return cc_cells


def get_cells_from_several_animals(cc_cells, animal_th=3, cell_th=20, fdr_alpha=0.01, method='hist',
                                   score='mean_score'):
    # get a pivot with the number of cells for each cell type for each animal
    piv_animals = cc_cells.pivot_table(index=['tissue', 'cell_ontology_class'], columns=['age', 'mouse.id'],
                                       values='cell', aggfunc='count').reindex(['3m', '18m', '24m'], axis=1,
                                                                               level='age')
    if method == 'two_norms':
        suf = ''
    else:
        suf = '_hist'
    if score == 'combine':
        cc_cells['new_fdr_flag'] = (cc_cells['fdr{}_{}'.format(suf, 'mean_G1_S')] <= fdr_alpha) | (
                cc_cells['fdr{}_{}'.format(suf, 'mean_G2_M')] <= fdr_alpha)
    else:
        cc_cells['new_fdr_flag'] = (cc_cells['fdr{}_{}'.format(suf, score)] <= fdr_alpha)

    cc_animals = cc_cells.pivot_table(index=['tissue', 'cell_ontology_class'], columns=['age', 'mouse.id'],
                                      values='new_fdr_flag', aggfunc='sum').reindex(['3m', '18m', '24m'], axis=1,
                                                                                    level='age')
    num_of_animals = piv_animals.shape[1] - np.array([animal_th])  # np.arange(1,piv_animals.shape[1]+1,4)
    anim_counts = piv_animals.quantile(num_of_animals / piv_animals.shape[1], interpolation='lower', axis=1).T
    anim_counts.columns = piv_animals.shape[1] - num_of_animals
    inds = anim_counts.index[anim_counts[animal_th] > cell_th]
    # only_above_melt = (cc_animals / piv_animals)[piv_animals.loc[inds] > cell_th].reset_index(level=(0, 1)).melt(
    #     id_vars=['tissue', 'cell_ontology_class']).dropna()
    cc_an = cc_animals[piv_animals.loc[inds] > cell_th].reset_index(level=(0, 1)).melt(
        id_vars=['tissue', 'cell_ontology_class'], value_name='cc_pos').dropna()
    tot_an = piv_animals[piv_animals.loc[inds] > cell_th].reset_index(level=(0, 1)).melt(
        id_vars=['tissue', 'cell_ontology_class'], value_name='total_cells').dropna()
    cc_an['total_cells'] = tot_an.total_cells
    cc_an['cc_frac'] = cc_an.cc_pos / cc_an.total_cells
    cc_an['cc_std'] = (cc_an.cc_frac * (1 - cc_an.cc_frac) / cc_an.total_cells) ** 0.5
    return cc_an


def get_cells_from_animals_to_compare(cc_cells, animal_th=3, cell_th=20, fdr_alpha=0.01, method='hist',
                                      score='mean_score',
                                      lit_data_file='../Datasets/Unlabeled/Our dataset/tissues summary 220309.xlsx'):
    cc_an = get_cells_from_several_animals(cc_cells, animal_th=animal_th, cell_th=cell_th, fdr_alpha=fdr_alpha,
                                           method=method, score=score)
    cc_an_for_comp = cc_an.pivot_table(index=['tissue', 'cell_ontology_class'], values=['cc_frac', 'cc_std'],
                                       aggfunc='median')

    lit_data = pd.read_excel(lit_data_file, index_col=[0, 1])
    lit_data['lifespan_for'] = lit_data['geom lifespan']
    lit_data.loc[lit_data.lifespan_for.isnull(), 'lifespan_for'] = lit_data.loc[
        lit_data.lifespan_for.isnull(), 'lifespan']

    cc_an_for_comp['lit_lifespan'] = lit_data['lifespan_for']
    cc_an_for_comp = cc_an_for_comp[(cc_an_for_comp.cc_frac > 0) & (cc_an_for_comp.lit_lifespan > 0)]
    cc_an_for_comp['day_turnover'] = 1 / cc_an_for_comp['cc_frac']
    cc_an_for_comp['day_turnover_sd'] = cc_an_for_comp.cc_std / cc_an_for_comp.cc_frac ** 2
    return cc_an_for_comp

#
# def get_cells_from_animals_to_compare(cc_cells, animal_th=3, cell_th=20, fdr_alpha=0.01, method='hist',
#                                       score='mean_score',
#                                       lit_data_file='../Datasets/Unlabeled/Our dataset/tissues summary 220309.xlsx'):
#     cc_an = get_cells_from_several_animals(cc_cells, animal_th=animal_th, cell_th=cell_th, fdr_alpha=fdr_alpha,
#                                            method=method, score=score)
#     cc_an_for_comp = cc_an.pivot_table(index=['tissue', 'cell_ontology_class'], values=['cc_frac', 'cc_std'],
#                                        aggfunc='median')
#
#     lit_data = pd.read_excel(lit_data_file, index_col=[0, 1])
#     lit_data['lifespan_for'] = lit_data['geom lifespan']
#     lit_data.loc[lit_data.lifespan_for.isnull(), 'lifespan_for'] = lit_data.loc[
#         lit_data.lifespan_for.isnull(), 'lifespan']
#
#     cc_an_for_comp['lit_lifespan'] = lit_data['lifespan_for']
#     cc_an_for_comp = cc_an_for_comp[(cc_an_for_comp.cc_frac > 0) & (cc_an_for_comp.lit_lifespan > 0)]
#     cc_an_for_comp['day_turnover'] = 1 / cc_an_for_comp['cc_frac']
#     cc_an_for_comp['day_turnover_sd'] = cc_an_for_comp.cc_std / cc_an_for_comp.cc_frac ** 2
#     return cc_an_for_comp