
from anndata import AnnData
from anndata import read_h5ad
import scanpy as sc

data_dir = 'C:/Users/ronse/Dropbox (Weizmann Institute)/scRNA cell analysis/Tabula Muris Senis'
# print(data_dir)

"""
This function get a .h5ad filename and import it using scanpy read_h5ad
and built an Anndata object
___
Inputs:
    * tissue_name - string with the name of the tissue
    * method - 'facs' or 'droplets'
___
Output:
    Anndata object with the scRNA data of the tissue
"""


def load_tissue_as_ad(tissue_name, method='facs'):
    ad = sc.read_h5ad(
        '{}/tabula-muris-senis-{}-processed-official-annotations-{}.h5ad'.format(data_dir, method, tissue_name))
    ad.var_names = ad.var_names.str.upper()
    ad.var_names_make_unique()
    return ad


"""
This function preprocess the scRNA data dropping cells and genes without minimal number of expressions.
It also scale the data, and use log transformation if needed
___
Inputs:
    * ad - Anndata object with the scRNA data
    * min_cells - threshold for number of cells
    * min_genes - threshold for number of genes
    * min_counts  - threshold for number of counts
    * log_scale - whether to log1p transform the data
    * zero_scale - whether to scale all the genes to have a mean of 0 and std of 1
___
Output:
    Anndata object after preprocess
"""


def preprocess_ad(ad, min_cells=5, min_genes=500, min_counts=5000, log_scale=False, zero_scale=True):
    sc.pp.filter_genes(ad, min_cells=min_cells)
    sc.pp.filter_cells(ad, min_genes=min_genes)
    ad = ad[ad.obs.n_counts > min_counts]
    # sc.pp.normalize_per_cell(skin_ad, counts_per_cell_after=1e4)
    ad = sc.pp.filter_genes_dispersion(ad, subset=False, min_disp=.5, max_disp=None, min_mean=.0125, max_mean=10,
                                       n_bins=20, n_top_genes=None, log=True, copy=True)
    if log_scale:
        sc.pp.log1p(ad)
    if zero_scale:
        sc.pp.scale(ad, zero_center=True)
    return ad
